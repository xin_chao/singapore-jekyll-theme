---
layout: blog
title: news
image: /images/blog/mystery.png
---


<p>
&nbsp;
</p>


{% for post in site.posts %}
  {% if post.category contains page.title or post.categories contains page.title %}
  <h3 class="cat-post-title">
  <a href="{{ site.url }}{{ site.baseurl }}{{ post.url }}">{{ post.title | capitalize }}</a>, <em>{{ post.date | date_to_string }}</em>
  </h3>
  <p>
  {{ post.summary }}
  &nbsp;
  <a href="{{ site.url }}{{ site.baseurl }}{{ post.url }}">[...]</a>
  </p>
  {% endif %}
{% endfor %}
